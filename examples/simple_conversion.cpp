//
// Created by nmeritn on 2018-06-20.
//

#include <iostream>
#include "../src/pipeline.h"
#include "../src/convert.h"

class source final : public pipeline::source_base<float> {
public:
    void do_push(float value) {
        push(value);
    }
};

class sink final : public pipeline::sink_base<int> {
public:
    void accept(const int &value) override {
        std::cout << value << std::endl;
    }
};

int main() {
    auto _source(pipeline::create<source>());
    auto _sink(pipeline::create<sink>());
    _source | pipeline::convert<int> | _sink;
    _source.get<source>()->do_push(7.5);
}
