//
// Created by nmeritn on 2018-06-07.
//

#include <iostream>
#include "../src/pipeline.h"

class source final : public pipeline::source_base<int> {
public:
    void do_push(int value) {
        push(value);
    }
};

class sink final : public pipeline::sink_base<int> {
public:
    void accept(const int &value) override {
        std::cout << value << std::endl;
    }
};

int foo(int x) {
    return x * x;
}

int main() {
    auto _source(pipeline::create<source>());
    _source |= foo;
    auto _sink(pipeline::create<sink>());
    _source | _sink;
    _source.get<source>()->do_push(7);
    _source.unbind_right();
    7 | _sink;
}
