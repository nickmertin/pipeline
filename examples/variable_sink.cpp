//
// Created by nmeritn on 2018-06-11.
//

#include <iostream>
#include "../src/pipeline.h"

class source final : public pipeline::source_base<int> {
public:
    void do_push(int value) {
        push(value);
    }
};

int main() {
    auto _source(pipeline::create<source>());
    int out;
    auto _sink(pipeline::create<pipeline::variable_sink<int>>(out));
    _source | _sink;
    _source.get<source>()->do_push(7);
    std::cout << out << std::endl;
}
