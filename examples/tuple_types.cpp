//
// Created by nmeritn on 2018-06-11.
//

#include <iostream>
#include "../src/pipeline.h"
#include "../src/tie.h"

class int_source final : public pipeline::source_base<int> {
public:
    void do_push(int x) {
        push(x);
    }
};

class float_source final : public pipeline::source_base<float> {
public:
    void do_push(float x) {
        push(x);
    }
};

class sink final : public pipeline::sink_base<std::tuple<int, float>> {
public:
    void accept(const std::tuple<int, float> &value) override {
        std::cout << "<" << std::get<0>(value) << ", " << std::get<1>(value) << ">" << std::endl;
    }
};

class int_sink final : public pipeline::sink_base<int> {
public:
    void accept(const int &value) override {
        std::cout << "Int: " << value << std::endl;
    }
};

class source final : public pipeline::source_base<std::tuple<int, float>> {
public:
    void do_push(const std::tuple<int, float> &x) {
        push(x);
    }
};

int main() {
    auto _int(pipeline::create<int_source>());
    auto _float(pipeline::create<float_source>());
    auto _sink(pipeline::create<sink>());
    auto _int2(pipeline::create<int_sink>());
    auto _source(pipeline::create<source>());
    pipeline::tie(_int, _float) | _sink;
    _int.get<int_source>()->do_push(7);
    _float.get<float_source>()->do_push(2.5);
    _source | pipeline::tie(_int2, [] (float value) { std::cout << "Float: " << value << std::endl; });
    _source.get<source>()->do_push(std::make_tuple(3, 7.5));
}
