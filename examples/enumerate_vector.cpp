//
// Created by nmeritn on 2018-06-11.
//

#include <vector>
#include <iostream>
#include "../src/pipeline.h"

class source final : public pipeline::source_base<std::vector<int>> {
public:
    void do_push(const std::vector<int> &vector) {
        push(vector);
    }
};

int main() {
    auto _source(pipeline::create<source>());
    auto values(_source | pipeline::enumerate);
    values | [] (int x) { std::cout << x << std::endl; };
    _source.get<source>()->do_push({1, 2, 3});
}
