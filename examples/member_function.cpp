//
// Created by nmeritn on 2018-06-10.
//

#include <iostream>
#include "../src/pipeline.h"

class object final {
public:
    int value;

    explicit object(int value) : value(value) {}

    void print() const {
        std::cout << value << std::endl;
    }
};

class source final : public pipeline::source_base<object> {
public:
    void do_push(object value) {
        push(value);
    }
};

int main() {
    auto _source(pipeline::create<source>());
    _source | &object::print;
    _source.get<source>()->do_push(object(7));
}
