//
// Created by nmeritn on 2018-06-10.
//

#ifndef PIPELINE_TRANSFORM_BASE_H
#define PIPELINE_TRANSFORM_BASE_H


#include "component_base.h"

namespace pipeline {
    template <class T, class U = T>
    class transform_base : public component_base<T, U> {
    private:
        virtual U transform(const T &value) = 0;

    public:
        void accept(const T &value) final {
            this->push(transform(value));
        }
    };

    template <class T>
    class transform_base<T, void> : public component_base<T, void> {
    private:
        virtual void transform(const T &value) = 0;

    public:
        void accept(const T &value) final {
            transform(value);
        }
    };
}


#endif //PIPELINE_TRANSFORM_BASE_H
