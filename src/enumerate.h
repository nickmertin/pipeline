//
// Created by nmeritn on 2018-06-11.
//

#ifndef PIPELINE_ENUMERATE_H
#define PIPELINE_ENUMERATE_H


#include "impl/enumerator_factory.h"

namespace pipeline {
    constexpr auto enumerate = component_factory<impl::enumerator_factory>::value;
}


#endif //PIPELINE_ENUMERATE_H
