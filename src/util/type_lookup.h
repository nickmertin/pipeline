//
// Created by nmeritn on 2018-06-16.
//

#ifndef PIPELINE_TYPE_LOOKUP_H
#define PIPELINE_TYPE_LOOKUP_H


#include <cstddef>

namespace pipeline::util {
    template <size_t I, class T, class ...Types>
    struct type_lookup_t : type_lookup_t<I - 1, Types...> {};

    template <class T, class ...Types>
    struct type_lookup_t<0, T, Types...> {
        using type = T;
    };

    template <size_t I, class ...Types>
    using type_lookup = typename type_lookup_t<I, Types...>::type;
}


#endif //PIPELINE_TYPE_LOOKUP_H
