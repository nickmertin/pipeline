//
// Created by nmeritn on 2018-06-19.
//

#ifndef PIPELINE_TYPE_CATEGORY_H
#define PIPELINE_TYPE_CATEGORY_H


#include <utility>

namespace pipeline::util {
    enum type_category_t {
        TYPE_UNKNOWN,
        TYPE_ARRAY,
        TYPE_CLASS,
        TYPE_ENUM,
        TYPE_FLOATING_POINT,
        TYPE_FUNCTION,
        TYPE_INTEGRAL,
        TYPE_LVALUE_REFERENCE,
        TYPE_MEMBER_FUNCTION_POINTER,
        TYPE_MEMBER_OBJECT_POINTER,
        TYPE_POINTER,
        TYPE_RVALUE_REFERENCE,
        TYPE_UNION,
        TYPE_VOID
    };

    template <class T>
    constexpr type_category_t type_category = std::is_array<T>::value ? TYPE_ARRAY : std::is_class<T>::value ? TYPE_CLASS : std::is_enum<T>::value ? TYPE_ENUM : std::is_floating_point<T>::value ? TYPE_FLOATING_POINT : std::is_function<T>::value ? TYPE_FUNCTION : std::is_integral<T>::value ? TYPE_INTEGRAL : std::is_lvalue_reference<T>::value ? TYPE_LVALUE_REFERENCE : std::is_member_function_pointer<T>::value ? TYPE_MEMBER_FUNCTION_POINTER : std::is_member_object_pointer<T>::value ? TYPE_MEMBER_OBJECT_POINTER : std::is_pointer<T>::value ? TYPE_POINTER : std::is_rvalue_reference<T>::value ? TYPE_RVALUE_REFERENCE : std::is_union<T>::value ? TYPE_UNION : std::is_void<T>::value ? TYPE_VOID : TYPE_UNKNOWN;
}


#endif //PIPELINE_TYPE_CATEGORY_H
