//
// Created by nmeritn on 2018-06-15.
//

#ifndef PIPELINE_TYPE_TERNARY_H
#define PIPELINE_TYPE_TERNARY_H


namespace pipeline::util {
    template <bool value, class A, class B>
    struct type_ternary_t {
        using type = A;
    };

    template <class A, class B>
    struct type_ternary_t<false, A, B> {
        using type = B;
    };

    template <bool value, class A, class B>
    using type_ternary = typename type_ternary_t<value, A, B>::type;
}


#endif //PIPELINE_TYPE_TERNARY_H
