//
// Created by nmeritn on 2018-06-16.
//

#ifndef PIPELINE_SEQUENCE_FILTER_H
#define PIPELINE_SEQUENCE_FILTER_H


namespace pipeline::util {
    template <class T, bool P, template <T ...> class O, T V, T ...I>
    struct sequence_filter_helper {
        using type = O<V, I...>;
    };

    template <class T, template <T ...> class O, T V, T ...I>
    struct sequence_filter_helper<T, false, O, V, I...> {
        using type = O<I...>;
    };

    template <class T, template <T> class P, template <T ...> class O, T ...I>
    struct sequence_filter_part {
        template <T ...J>
        using map = sequence_filter_part<T, P, O, J...>;

        template <T V>
        using extend = typename sequence_filter_helper<T, P<V>::value, map, V, I...>::type;

        using type = O<I...>;
    };

    template <class T, template <T> class P, template <T ...> class O, T ...I>
    struct sequence_filter_t;

    template <class T, template <T> class P, template <T ...> class O, T V, T ...I>
    struct sequence_filter_t<T, P, O, V, I...> {
        using type = typename sequence_filter_t<T, P, O, I...>::type::template extend<V>;
    };

    template <class T, template <T> class P, template <T ...> class O>
    struct sequence_filter_t<T, P, O> {
        using type = sequence_filter_part<T, P, O>;
    };

    template <class T, template <T> class P, template <T ...> class O, T ...I>
    using sequence_filter = typename sequence_filter_t<T, P, O, I...>::type::type;
}


#endif //PIPELINE_SEQUENCE_FILTER_H
