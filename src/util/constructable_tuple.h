//
// Created by nmeritn on 2018-06-12.
//

#ifndef PIPELINE_CONSTRUCTABLE_TUPLE_H
#define PIPELINE_CONSTRUCTABLE_TUPLE_H


#include <cstddef>
#include <tuple>

namespace pipeline::util {
    template <class ...T>
    class constructable_tuple;

    template <size_t, class ...>
    struct get_helper;

    template <class ...T>
    struct get_helper_end;

    template <class A, class ...T>
    class constructable_tuple<A, T...> final {
    private:
        template <size_t, class ...>
        friend struct get_helper;

        template <class ...>
        friend struct get_helper_end;

        class value_wrapper final {
        public:
            A value;

        private:
            template <class U, size_t ...I>
            value_wrapper(const U &args, std::index_sequence<I...>) : value(std::get<I>(args)...) {}

        public:
            template <class ...U>
            value_wrapper(const std::tuple<U...> &args) : value_wrapper(args, std::make_index_sequence<sizeof...(U)>()) {}
        };

        value_wrapper value;
        constructable_tuple<T...> rest;

        template <class F, class ...U>
        constructable_tuple(nullptr_t, const F &first, const U &...rest) : value(first), rest(rest...) {}

    public:
        template <class ...U>
        constructable_tuple(const U &...args) : constructable_tuple(nullptr, args...) {}
    };

    template <>
    class constructable_tuple<> final {
        // Intentionally empty
    };

    template <size_t I, class ...T>
    struct get_helper;

    template <size_t I, class A, class ...T>
    struct get_helper<I, A, T...> {
        using type = typename get_helper<I - 1, T...>::type;

        static auto &get(constructable_tuple<A, T...> *tuple) {
            return get_helper<I - 1, T...>::get(&tuple->rest);
        }
    };

    template <class ...T>
    struct get_helper_end {
        template <class A, class ...>
        struct helper {
            using type = A;
        };

        using type = typename helper<T...>::type;

        static auto &get(constructable_tuple<T...> *tuple) {
            return tuple->value.value;
        }
    };

    template <class ...T>
    struct get_helper<0, T...> : get_helper_end<T...> {};

    template <class A, class ...T>
    struct get_helper<0, A, T...> : get_helper_end<A, T...> {};

    template <size_t I, class ...T>
    auto &get(constructable_tuple<T...> &tuple) {
        return get_helper<I, T...>::get(&tuple);
    }
}


#endif //PIPELINE_CONSTRUCTABLE_TUPLE_H
