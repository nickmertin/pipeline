//
// Created by nmeritn on 2018-06-11.
//

#ifndef PIPELINE_TUPLE_CONVERSION_H
#define PIPELINE_TUPLE_CONVERSION_H


#include <tuple>

namespace pipeline::util {
    template <template <class, class> class, class>
    struct deconstruct_t;

    template <template <class, class> class O, class T, class U>
    struct deconstruct_t<O, O<T, U>> {
        using left = T;

        using right = U;
    };

    template <template <class, class> class O, class A>
    using left_type = typename deconstruct_t<O, typename std::decay<A>::type>::left;

    template <template <class, class> class O, class A>
    using right_type = typename deconstruct_t<O, typename std::decay<A>::type>::right;

    template <template <class, class> class O, class ...Types>
    using left_tuple = std::tuple<typename std::decay<left_type<O, typename std::decay<Types>::type>>::type...>;

    template <template <class, class> class O, class ...Types>
    using right_tuple = std::tuple<typename std::decay<right_type<O, typename std::decay<Types>::type>>::type...>;

    template <class ...Types>
    struct voidless_tuple_t;

    template <>
    struct voidless_tuple_t<> {
        using type = std::tuple<>;
    };

    template <class ...Types>
    struct voidless_tuple_t<void, Types...> {
        using type = typename voidless_tuple_t<Types...>::type;
    };

    template <class T, class ...Types>
    struct voidless_tuple_t<T, Types...> {
        template <size_t ...I>
        static std::tuple<T, typename std::tuple_element<I, typename voidless_tuple_t<Types...>::type>::type...> helper(std::index_sequence<I...>);

        using type = decltype(helper(std::make_index_sequence<sizeof...(Types)>()));
    };

    template <class ...T>
    using voidless_tuple = typename voidless_tuple_t<typename std::decay<T>::type...>::type;
}


#endif //PIPELINE_TUPLE_CONVERSION_H
