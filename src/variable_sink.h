//
// Created by nmeritn on 2018-06-11.
//

#ifndef PIPELINE_VARIABLE_SINK_H
#define PIPELINE_VARIABLE_SINK_H


#include "component_base.h"

namespace pipeline {
    template <class T>
    class variable_sink final : public sink_base<T> {
    private:
        T &out;

    public:
        explicit variable_sink(T &out) : out(out) {}

        variable_sink(const variable_sink &other) : out(other.out) {}

        void accept(const T &value) override {
            out = value;
        }
    };
}


#endif //PIPELINE_VARIABLE_SINK_H
