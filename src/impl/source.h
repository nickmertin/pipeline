//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_SOURCE_H
#define PIPELINE_SOURCE_H


#include "../component_base.h"
#include "source_data.h"
#include "type_map.h"
#include "../component_factory.h"
#include "../util/tuple_conversion.h"
#include "pipe.h"

namespace pipeline::impl {
    template <class T, class U>
    class source {
    private:
        template <class A>
        static auto left_destructor(sink_data<U, A> *_right, const std::shared_ptr<bool> &cont) {
            return [_right, cont] {
                if (*cont && !_right->_binding->destructing) {
                    *cont = false;
                    _right->_binding.reset();
                }
            };
        }

        template <class TBinding>
        static auto right_destructor(source_data<T, U> *_left, const std::shared_ptr<bool> &cont, std::function<bool(TBinding *)> predicate) {
            return [_left, cont, predicate] {
                if (*cont) {
                    *cont = false;
                    auto itt(_left->bindings.begin());
                    auto end(_left->bindings.end());
                    while (itt != end) {
                        auto b(dynamic_cast<TBinding *>(itt->get()));
                        if (b && predicate(b)) {
                            if (!b->destructing)
                                _left->bindings.erase(itt);
                            break;
                        }
                        ++itt;
                    }
                }
            };
        }
        
        template <class A>
        class pipe_right final {
        public:
            static component<T, A> pipe(const component<T, U> &left, const component<U, A> &right) {
                source_data<T, U> *_left = &left.get_source();
                sink_data<U, A> *_right = &right.get_sink();
                pipeline::component<T, A> result(
                        new typename component_data<T, A>::template pipe_result<U, PIPE_RIGHT>(left._data, right._data.get()));
                std::shared_ptr<bool> cont(new bool(true));
                _left->bindings.push_back(std::unique_ptr<typename source_data<T, U>::binding>(new typename source_data<T, U>::binding(left_destructor(_right, cont), &_right->_container->get_sink())));
                _right->_binding.reset(new typename sink_data<U, A>::template internal_binding<T>(right_destructor<typename source_data<T, U>::binding>(_left, cont, [_right](auto *b) { return b->_sink == &_right->_container->get_sink(); }), result._data));
                return result;
            }
        };

        template <class A>
        class pipe_left final {
        public:
            static component<T, A> pipe(const component<T, U> &left, const component<U, A> &right) {
                source_data<T, U> *_left = &left.get_source();
                sink_data<U, A> *_right = &right.get_sink();
                pipeline::component<T, A> result(new typename component_data<T, A>::template pipe_result<U, PIPE_LEFT>(left._data.get(), right._data));
                auto _data(result._data.get());
                std::shared_ptr<bool> cont(new bool(true));
                _left->bindings.push_back(std::unique_ptr<typename source_data<T, U>::binding>(new typename source_data<T, U>::template internal_binding<A>(left_destructor(_right, cont), &_right->_container->get_sink(), result._data)));
                _right->_binding.reset(new typename sink_data<U, A>::binding(right_destructor<typename source_data<T, U>::template internal_binding<A>>(_left, cont, [_data](auto *b) { return b->_data.get() == _data; })));
                return result;
            }
        };

        template <class A>
        class pipe_none final {
        public:
            static component<T, A> pipe(const component<T, U> &left, const component<U, A> &right) {
                source_data<T, U> *_left = &left.get_source();
                sink_data<U, A> *_right = &right.get_sink();
                pipeline::component<T, A> result(new typename component_data<T, A>::template pipe_result<U, PIPE_NONE>(left._data, right._data));
                std::shared_ptr<bool> cont(new bool(true));
                _left->bindings.push_back(std::unique_ptr<typename source_data<T, U>::binding>(new typename source_data<T, U>::binding(left_destructor(_right, cont), &_right->_container->get_sink())));
                _right->_binding.reset(new typename sink_data<U, A>::binding(right_destructor<typename source_data<T, U>::binding>(_left, cont, [_right] (auto *b) { return b->_sink == &_right->_container->get_sink(); })));
                return result;
            }
        };

    public:
        void unbind_right() const {
            dynamic_cast<const component<T, U> *>(this)->get_source().bindings.clear();
        }

        virtual ~source() = default;

        template <class R>
        friend auto operator|(const component<T, U> &left, const R &right) {
            return pipe<source<T, U>::template pipe_right, source<T, U>::template pipe_left>(left, right);
        }

        template <class R>
        friend auto operator|(component<T, U> &left, const R &right) {
            return pipe<source<T, U>::template pipe_right, source<T, U>::template pipe_left>(left, right);
        }

        template <class R>
        friend auto operator|(const component<T, U> &left, R &&right) {
            return pipe<source<T, U>::template pipe_left, source<T, U>::template pipe_left>(left, right);
        }

        template <class R>
        friend auto operator|(component<T, U> &left, R &&right) {
            return pipe<source<T, U>::template pipe_left, source<T, U>::template pipe_left>(left, right);
        }

        template <class R>
        friend auto operator|(component<T, U> &&left, const R &right) {
            return pipe<source<T, U>::template pipe_right, source<T, U>::template pipe_none>(left, right);
        }

        template <class R>
        friend auto operator|(component<T, U> &&left, R &right) {
            return pipe<source<T, U>::template pipe_right, source<T, U>::template pipe_none>(left, right);
        }

        template <class R>
        friend auto operator|(component<T, U> &&left, R &&right) {
            return pipe<source<T, U>::template pipe_none, source<T, U>::template pipe_none>(left, right);
        }

        template <class R>
        friend component<T, U> operator|=(component<T, U> &left, const R &right) {
            return left = pipe<source<T, U>::template pipe_none, source<T, U>::template pipe_none>(left, right);
        }
    };

    template <class T>
    class source<T, void> {
        // Intentionally empty
    };
}


#endif //PIPELINE_SOURCE_H
