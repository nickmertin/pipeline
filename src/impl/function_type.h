//
// Created by nmeritn on 2018-06-10.
//

#ifndef PIPELINE_FUNCTION_TYPE_H
#define PIPELINE_FUNCTION_TYPE_H


#include <functional>

namespace pipeline::impl {
    struct null_type {};

    template <class T, class U>
    struct function_type_t {
        using type = std::function<U(const T &)>;
    };

    template <class T>
    struct function_type_t<void, T> {
        using type = std::function<const T &()>;
    };

    template <>
    struct function_type_t<void, void> {
        using type = null_type;
    };

    template <class T, class U>
    using function_type = typename function_type_t<T, U>::type;

    template <class T, class U, bool is_valid>
    struct method_ptr_type_t {
        using type = U (T::* const)() const;
    };

    template <class T, class U>
    struct method_ptr_type_t<T, U, false> {
        using type = null_type;
    };

    template <class T, class U>
    using method_ptr_type = typename method_ptr_type_t<T, U, std::is_class<T>::value>::type;
}


#endif //PIPELINE_FUNCTION_TYPE_H
