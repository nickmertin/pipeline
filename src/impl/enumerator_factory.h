//
// Created by nmeritn on 2018-06-11.
//

#ifndef PIPELINE_ENUMERATOR_FACTORY_H
#define PIPELINE_ENUMERATOR_FACTORY_H


#include <type_traits>
#include "../create.h"

namespace pipeline::impl {
    class enumerator_factory final {
    public:
        template <class T>
        static auto create() {
            static_assert(!std::is_void<T>::value);

            struct _ {
                static auto helper(T t) {
                    return *t.begin();
                }
            };

            using U = typename std::decay<typename std::result_of<decltype(_::helper)&(T)>::type>::type;

            class enumerator final : public component_base<T, U> {
            public:
                void accept(const T &value) override {
                    for (const U &u : value)
                        this->push(u);
                }
            };

            return pipeline::create<enumerator>();
        }
    };
}


#endif //PIPELINE_ENUMERATOR_FACTORY_H
