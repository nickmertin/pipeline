//
// Created by nmeritn on 2018-06-07.
//

#ifndef PIPELINE_IMPL_CREATE_H
#define PIPELINE_IMPL_CREATE_H


#include "../component.h"

namespace pipeline::impl {
    template <class T, class U>
    component<T, U> create(component_base<T, U> *value) {
        return typename pipeline::component<T, U>(new typename component_data<T, U>::single_component(value));
    }
}


#endif //PIPELINE_IMPL_CREATE_H
