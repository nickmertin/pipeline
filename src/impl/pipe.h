//
// Created by nmeritn on 2018-06-19.
//

#ifndef PIPELINE_PIPE_H
#define PIPELINE_PIPE_H


#include "type_map.h"
#include "../util/type_ternary.h"
#include "../component_factory.h"

namespace pipeline::impl {
    template <class, class R>
    struct pipe_t {
        using type = source_type<R>;

        static constexpr bool store = store_type<R>;

        static auto convert(const R &value) {
            return convert_type(value);
        }
    };

    template <class U, class F>
    struct pipe_t<U, component_factory<F>> {
        using type = util::right_type<component, decltype(F::template create<U>())>;

        static constexpr bool store = false;

        static auto convert(component_factory<F>) {
            return F::template create<U>();
        }
    };

    template <template <class> class pipe_store, template <class> class pipe_no_store, class T, class U, class R>
    auto pipe(const component<T, U> &left, const R &right) {
        using t = pipe_t<U, typename std::decay<R>::type>;

        return util::type_ternary<t::store, pipe_store<typename t::type>, pipe_no_store<typename t::type>>::pipe(left, t::convert(right));
    }
}


#endif //PIPELINE_PIPE_H
