//
// Created by nmeritn on 2018-06-20.
//

#ifndef PIPELINE_CONVERSION_FACTORY_H
#define PIPELINE_CONVERSION_FACTORY_H


#include <type_traits>
#include "../create.h"

namespace pipeline::impl {
    template <class U>
    class conversion_factory final {
    public:
        template <class T>
        static auto create() {
            static_assert(!std::is_void<T>::value);

            class conversion final : public component_base<T, U> {
            public:
                void accept(const T &value) override {
                    this->push((U) value);
                }
            };

            return pipeline::create<conversion>();
        }
    };
}


#endif //PIPELINE_CONVERSION_FACTORY_H
