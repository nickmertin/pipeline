//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_SINK_DATA_H
#define PIPELINE_SINK_DATA_H


#include <functional>
#include "component_data.h"

namespace pipeline {
    template <class, class>
    class component;
}

namespace pipeline::impl {
    template <class, class>
    class sink;

    template <class, class>
    class data;

    template <class T, class U>
    class sink_data : public virtual component_data<T, U> {
    public:
        class binding {
        protected:
            std::function<void()> destroy;

        public:
            bool destructing = false;

            explicit binding(const std::function<void()> &destroy) : destroy(destroy) {}

            virtual ~binding() {
                destructing = true;
                if (destroy)
                    destroy();
            }
        };

        template <class A>
        class internal_binding final : public binding {
        private:
            std::shared_ptr<data<A, U>> _data;

        public:
            internal_binding(const std::function<void()> &destroy, const std::shared_ptr<data<A, U>> &_data) : binding(destroy), _data(_data) {}

            ~internal_binding() override {
                this->destructing = true;
                this->destroy();
                this->destroy = nullptr;
            }
        };

        std::unique_ptr<binding> _binding;

        sink_data() : component_data<T, U>(nullptr) {}
    };

    template <class T>
    class sink_data<void, T> {
        // Intentionally empty
    };
}


#endif //PIPELINE_SINK_DATA_H
