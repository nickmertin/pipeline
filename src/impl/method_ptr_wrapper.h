//
// Created by nmeritn on 2018-06-10.
//

#ifndef PIPELINE_METHOD_PTR_WRAPPER_H
#define PIPELINE_METHOD_PTR_WRAPPER_H


#include "function_type.h"

namespace pipeline::impl {
    template <class T, class U>
    class method_ptr_wrapper final {
    private:
        method_ptr_type<T, U> ptr;

    public:
        explicit method_ptr_wrapper(method_ptr_type<T, U> ptr) : ptr(ptr) {}

        U operator()(const T &value) {
            return (value.*ptr)();
        }
    };
}


#endif //PIPELINE_METHOD_PTR_WRAPPER_H
