//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_SINK_BASE_H
#define PIPELINE_SINK_BASE_H


#include "source_base.h"

namespace pipeline::impl {
    template <class>
    class source_base;

    template <class T>
    class sink_base {
    private:
        friend class source_base<T>;

        source_base<T> *_source;

    public:
        sink_base() noexcept : _source(nullptr) {}

        sink_base(const sink_base<T> &) noexcept : _source(nullptr) {}

        sink_base(sink_base<T> &&other) noexcept : _source(other._source) {
            if (_source)
                _source->_sink = this;
        }

        void bind(source_base<T> *_source) {
            if (this->_source)
                this->_source->_sink = nullptr;
            this->_source = _source;
            _source->_sink = this;
        }

        virtual void accept(const T &value) = 0;

        virtual ~sink_base() {
            if (_source)
                _source->_sink = nullptr;
        }
    };

    template <>
    class sink_base<void> {
    public:
        virtual ~sink_base() = default;
    };
}


#endif //PIPELINE_SINK_BASE_H
