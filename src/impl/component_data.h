//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_COMPONENT_DATA_H
#define PIPELINE_COMPONENT_DATA_H


#include <memory>
#include "../component_base.h"

namespace pipeline::impl {
    template <class, class>
    class data;

    template <class T, class U, bool shared>
    struct data_ptr_t {
        using type = data<T, U> *;
    };

    template <class T, class U>
    struct data_ptr_t<T, U, true> {
        using type = std::shared_ptr<data<T, U>>;
    };

    template <class T, class U, bool shared>
    using data_ptr = typename data_ptr_t<T, U, shared>::type;

    enum pipe_storage {
        PIPE_NONE,
        PIPE_RIGHT,
        PIPE_LEFT
    };

    template <class T, class U>
    class component_data {
    public:
        class container {
        public:
            virtual impl::sink_base<T> &get_sink() = 0;

            virtual impl::source_base<U> &get_source() = 0;

            container() = default;

            virtual ~container() {}
        };

        class single_component final : public container {
        public:
            std::unique_ptr<pipeline::component_base<T, U>> value;

            explicit single_component(pipeline::component_base<T, U> *value) : value(value) {}

            impl::sink_base<T> &get_sink() override {
                return *dynamic_cast<impl::sink_base<T> *>(value.get());
            }

            impl::source_base<U> &get_source() override {
                return *dynamic_cast<impl::source_base<U> *>(value.get());
            }
        };

        template <class A, pipe_storage storage>
        class pipe_result final : public container {
        public:
            using left_ptr = data_ptr<T, A, storage != PIPE_LEFT>;
            using right_ptr = data_ptr<A, U, storage != PIPE_RIGHT>;

            left_ptr left;
            right_ptr right;

            pipe_result(const left_ptr &left, const right_ptr &right) : left(left), right(right) {}

            sink_base<T> &get_sink() override {
                return left->_container->get_sink();
            }

            source_base<U> &get_source() override {
                return right->_container->get_source();
            }
        };

        class source_only final : public component_data<void, U>::container {
        public:
            std::shared_ptr<data<T, U>> _data;

            source_only(const std::shared_ptr<data<T, U>> &_data) : _data(_data) {}

            sink_base<void> &get_sink() override {
                return *static_cast<sink_base<void> *>(nullptr);
            }

            source_base <U> &get_source() override {
                return _data->_container->get_source();
            }
        };

        class sink_only final : public component_data<T, void>::container {
        public:
            std::shared_ptr<data<T, U>> _data;

            sink_only(const std::shared_ptr<data<T, U>> &_data) : _data(_data) {}

            sink_base <T> &get_sink() override {
                return _data->_container->get_sink();
            }

            source_base<void> &get_source() override {
                return *static_cast<source_base<void> *>(nullptr);
            }
        };

        std::unique_ptr<container> _container;

        explicit component_data(container *_container) : _container(_container) {}

        virtual ~component_data() {}
    };
}


#endif //PIPELINE_COMPONENT_DATA_H
