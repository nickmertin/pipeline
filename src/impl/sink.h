//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_SINK_H
#define PIPELINE_SINK_H


#include "../component_base.h"
#include "sink_data.h"
#include "../component.h"
#include "create.h"

namespace pipeline::impl {
    template <class T, class U>
    class sink {
    private:
        static component<void, U> pipe(const T &left, const component<T, U> &right) {
            right.unbind_left();
            right._data->_container->get_sink().accept(left);
            return component<void, U>(new typename component_data<T, U>::source_only(right._data));
        }

    public:
        void unbind_left() const {
            dynamic_cast<const component<T, U> *>(this)->get_sink()._binding.reset();
        }

        virtual ~sink() = default;

        friend component<void, U> operator|(const T &left,  const component<T, U> &right) {
            return sink<T, U>::pipe(left, right);
        }
    };

    template <class U>
    class sink<void, U> {
        // Intentionally empty
    };
}


#endif //PIPELINE_SINK_H
