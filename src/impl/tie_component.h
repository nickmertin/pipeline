//
// Created by nmeritn on 2018-06-16.
//

#ifndef PIPELINE_TIE_COMPONENT_H
#define PIPELINE_TIE_COMPONENT_H


#include "../component_base.h"
#include "../util/constructable_tuple.h"
#include "../util/type_lookup.h"
#include "../util/sequence_filter.h"
#include "type_map.h"

namespace pipeline::impl {
    template <class ...Types>
    using tie_type = util::type_ternary<(... && std::is_void<Types>::value), void, util::voidless_tuple <Types...>>;

    template <class ...Types>
    using tie_sink = tie_type<sink_type<Types>...>;

    template <class ...Types>
    using tie_source = tie_type<source_type<Types>...>;

    template <class ...Types>
    using tie_result = component<tie_sink<Types...>, tie_source<Types...>>;

    template <bool, class ...>
    class tie_component;

    template <class T, class U>
    class tie_container_base {
    public:
        std::shared_ptr<data<T, U>> _data;

        explicit tie_container_base(const std::shared_ptr<data<T, U>> &_data) : _data(_data) {}
    };

    template <class T, class U>
    class tie_container_base2 : public tie_container_base<T, U> {
    private:
        sink_base<T> *_sink;

    public:
        void accept(const T &value) {
            if (_sink)
                _sink->accept(value);
        }

        explicit tie_container_base2(const std::shared_ptr<data<T, U>> &_data) : tie_container_base<T, U>(_data), _sink(&_data->_container->get_sink()) {
            _data->_binding.reset(new typename sink_data<T, U>::binding([this] {
                if (_sink) {
                    _sink = nullptr;
                    this->_data.reset();
                }
            }));
        }

        virtual ~tie_container_base2() {
            if (_sink) {
                this->_data->_binding.reset();
            }
        }
    };

    template <class T>
    class tie_container_base2<void, T> : public tie_container_base<void, T> {
    public:
        explicit tie_container_base2(const std::shared_ptr<data<void, T>> &_data) : tie_container_base<void, T>(_data) {}
    };

    template <bool aggressive_push, class T, class U, class ...Types>
    class tie_container final : public tie_container_base2<T, U>, public sink_base<U> {
    private:
        tie_component<aggressive_push, Types...> *parent;
        bool connected = true;

    public:
        std::unique_ptr<U> value;

        void accept(const U &value) {
            this->value.reset(new U(value));
            parent->try_push(parent->sink_indices(std::make_index_sequence<sizeof...(Types)>()));
        }

        explicit tie_container(const std::shared_ptr<data<T, U>> &_data, tie_component<aggressive_push, Types...> *parent) : tie_container_base2<T, U>(_data), parent(parent), value(nullptr) {
            _data->bindings.push_back(std::unique_ptr<typename source_data<T, U>::binding>(new typename source_data<T, U>::binding([this] {
                if (connected) {
                    connected = false;
                    auto itt(this->_data->bindings.begin());
                    auto end(this->_data->bindings.end());
                    while (itt != end) {
                        auto b(itt->get());
                        if (b && b->_sink == dynamic_cast<sink_base<U> *>(this)) {
                            if (!b->destructing)
                                this->_data->bindings.erase(itt);
                            break;
                        }
                        ++itt;
                    }
                }
            }, dynamic_cast<sink_base<U> *>(this))));
        }

        virtual ~tie_container() {
            if (connected) {
                auto itt(this->_data->bindings.begin());
                auto end(this->_data->bindings.end());
                while (itt != end) {
                    auto b(itt->get());
                    if (b && b->_sink == dynamic_cast<sink_base<U> *>(this)) {
                        if (!b->destructing)
                            this->_data->bindings.erase(itt);
                        break;
                    }
                    ++itt;
                }
            }
        }
    };

    template <bool aggressive_push, class T, class ...Types>
    class tie_container<aggressive_push, T, void, Types...> final : public tie_container_base2<T, void> {
    public:
        explicit tie_container(const std::shared_ptr<data<T, void>> &_data, tie_component<aggressive_push, Types...> *) : tie_container_base2<T, void>(_data) {}
    };

    template <bool aggressive_push, class T, class U, class ...Types>
    class tie_component_base : public component_base<T, U> {
    public:
        util::constructable_tuple<tie_container<aggressive_push, util::left_type<component, Types>, util::right_type<component, Types>, Types...>...> containers;

        explicit tie_component_base(tie_component<aggressive_push, Types...> *_this, const Types &...args) : containers(std::tie(args._data, _this)...) {}

        explicit tie_component_base();
    };

    template <bool aggressive_push, class T, class U, class ...Types>
    class tie_sink_component : public virtual tie_component_base<aggressive_push, T, U, Types...> {
    public:
        template <size_t I>
        struct is_source {
            static constexpr bool value = !std::is_void<util::type_lookup<I, util::left_type<component, Types>...>>::value;
        };

        template <size_t ...I>
        static constexpr auto source_indices(std::index_sequence<I...>) {
            return util::sequence_filter<size_t, is_source, std::index_sequence, I...>();
        }

    private:

        template <size_t ...I>
        auto get_components(std::index_sequence<I...>) {
            return std::tie(util::get<I>(this->containers)...);
        }

        template <size_t ...I>
        void accept(const T &value, std::index_sequence<I...> seq) {
            (..., std::get<I>(get_components(source_indices(seq))).accept(std::get<I>(value)));
        }

    public:
        tie_sink_component() : tie_component_base<aggressive_push, T, U, Types...>() {}

        void accept(const T &value) override {
            accept(value, std::make_index_sequence<sizeof...(Types)>());
        }
    };

    template <bool aggressive_push, class T, class ...Types>
    class tie_sink_component<aggressive_push, void, T, Types...> {};

    template <bool aggressive_push, class T, class U, class ...Types>
    class tie_source_component : public virtual tie_component_base<aggressive_push, T, U, Types...> {
    public:
        template <size_t I>
        struct is_sink {
            static constexpr bool value = !std::is_void<util::type_lookup<I, util::right_type<component, Types>...>>::value;
        };

        template <size_t ...I>
        static constexpr auto sink_indices(std::index_sequence<I...>) {
            return util::sequence_filter<size_t, is_sink, std::index_sequence, I...>();
        }

        tie_source_component() : tie_component_base<aggressive_push, T, U, Types...>() {}

        template <size_t ...I>
        void try_push(std::index_sequence<I...>) {
            if ((... || !util::get<I>(this->containers).value))
                return;
            this->push(std::make_tuple(*util::get<I>(this->containers).value...));
            if (!aggressive_push)
                (..., util::get<I>(this->containers).value.reset());
        }
    };

    template <bool aggressive_push, class T, class ...Types>
    class tie_source_component<aggressive_push, T, void, Types...> {};

    template <bool aggressive_push, class ...Types>
    class tie_component final : public tie_sink_component<aggressive_push, tie_sink<Types...>, tie_source<Types...>, Types...>, public tie_source_component<aggressive_push, tie_sink<Types...>, tie_source<Types...>, Types...>, public virtual tie_component_base<aggressive_push, tie_sink<Types...>, tie_source<Types...>, Types...> {
    public:
        tie_component(const Types &...args) : tie_component_base<aggressive_push, tie_sink<Types...>, tie_source<Types...>, Types...>(this, args...) {}
    };
}


#endif //PIPELINE_TIE_COMPONENT_H
