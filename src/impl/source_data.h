//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_SOURCE_DATA_H
#define PIPELINE_SOURCE_DATA_H


#include <list>
#include "component_data.h"

namespace pipeline::impl {
    template <class T, class U>
    class source_data : public virtual component_data<T, U> {
    public:
        class binding {
        protected:
            std::function<void()> destroy;

        public:
            sink_base<U> *_sink;
            bool destructing = false;

            binding(const std::function<void()> &destroy, sink_base <U> *_sink) : destroy(destroy), _sink(_sink) {}

            virtual ~binding() {
                this->destructing = true;
                if (destroy)
                    destroy();
            }
        };

        template <class A>
        class internal_binding final : public binding {
        private:
            template <class, class>
            friend class source;

            std::shared_ptr<data<T, A>> _data;

        public:
            internal_binding(const std::function<void()> &destroy, sink_base <U> *_sink, const std::shared_ptr<data<T, A>> &_data) : binding(destroy, _sink), _data(_data) {}

            ~internal_binding() override {
                this->destructing = true;
                this->destroy();
                this->destroy = nullptr;
            }
        };

        std::list<std::unique_ptr<binding>> bindings;

        void bind(sink_base<U> *_sink) {
            _sink->bind(&this->_container->get_source());
        }

        class out_sink final : sink_base<U> {
        public:
            source_data<T, U> *_data;

            explicit out_sink(source_data<T, U> *_data) : _data(_data) {
                _data->bind(dynamic_cast<sink_base<U> *>(this));
            }

            void accept(const U &value) override {
                for (std::unique_ptr<binding> &b : _data->bindings)
                    b->_sink->accept(value);
            }
        };

        out_sink out;

    public:
        source_data() : component_data<T, U>(nullptr), out(this) {}
    };

    template <class T>
    class source_data<T, void> {
        // Intentionally empty
    };
}


#endif //PIPELINE_SOURCE_DATA_H
