//
// Created by nmeritn on 2018-06-07.
//

#ifndef PIPELINE_DATA_H
#define PIPELINE_DATA_H


#include "sink_data.h"

namespace pipeline::impl {
    template <class T, class U>
    class data final : public sink_data<T, U>, public source_data<T, U>, public virtual component_data<T, U> {
    public:
        explicit data(typename component_data<T, U>::container *_container) : component_data<T, U>(_container) {}
    };
}


#endif //PIPELINE_DATA_H
