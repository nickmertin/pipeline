//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_SOURCE_BASE_H
#define PIPELINE_SOURCE_BASE_H


#include "sink_base.h"

namespace pipeline::impl {
    template <class>
    class sink_base;

    template <class, class>
    class source;

    template <class, class>
    class source_data;

    template <class T>
    class source_base {
    private:
        friend class sink_base<T>;

        sink_base<T> *_sink;

        void bind(sink_base<T> *_sink) {
            if (this->_sink)
                this->_sink->_source = nullptr;
            this->_sink = _sink;
            _sink->_source = this;
        }

    protected:
        void push(const T &value) {
            if (_sink)
                _sink->accept(value);
        }

    public:
        source_base() noexcept : _sink(nullptr) {}

        source_base(const source_base<T> &) noexcept : _sink(nullptr) {}

        source_base(source_base<T> &&other) noexcept : _sink(other._sink) {
            if (_sink)
                _sink->_source = this;
        }

        virtual ~source_base() noexcept {
            if (_sink)
                _sink->_source = nullptr;
        }
    };

    template <>
    class source_base<void> {
    public:
        virtual ~source_base() = default;
    };
}


#endif //PIPELINE_SOURCE_BASE_H
