//
// Created by nmeritn on 2018-06-10.
//

#ifndef PIPELINE_TYPE_MAP_H
#define PIPELINE_TYPE_MAP_H


#include <type_traits>
#include "function_type.h"
#include "../util/type_category.h"
#include "method_ptr_wrapper.h"

namespace pipeline {
    template <class, class>
    class component;
}

namespace pipeline::impl {
    template <class TValue, util::type_category_t category>
    struct type_map_base;

    template <class ...Types>
    struct type_map_base<std::tuple<Types...>, util::TYPE_CLASS> {};

    template <class TValue>
    struct type_map_base<TValue, util::TYPE_CLASS> {
        template <class T, class U>
        static T helper(U (TValue::*)(T) const);

        template <class T>
        static void helper(T (TValue::*)() const);

        using sink_t = decltype(helper(TValue::operator()));

        using source_t = typename std::result_of<TValue(sink_t)>::type;

        static constexpr bool store = false;

        static component<sink_t, source_t> convert(const TValue &value) {
            return component<sink_t, source_t>((function_type<sink_t, source_t>) value);
        };
    };

    template <class T, class U>
    struct type_map_base<component<T, U>, util::TYPE_CLASS> {
        using sink_t = T;

        using source_t = U;

        static constexpr bool store = true;

        static const component<T, U> &convert(const component<T, U> &value) {
            return value;
        };
    };

    template <class TValue>
    struct type_map_function_helper;

    template <class T, class U>
    struct type_map_function_helper<U (*)(T)> {
        using sink_t = T;

        using source_t = U;

        static constexpr bool store = false;

        static component<sink_t, source_t> convert(U (*value)(T)) {
            return component<T, U>((function_type<T, U>) value);
        };
    };

    template <class TValue>
    struct type_map_base<TValue, util::TYPE_FUNCTION> : type_map_function_helper<typename std::decay<TValue>::type> {};

    template <class TValue>
    struct type_map_member_function_helper;

    template <class T, class U>
    struct type_map_member_function_helper<U (T::*)() const> {
        using sink_t = T;

        using source_t = U;

        static constexpr bool store = false;

        static component<sink_t, source_t> convert(U (T::*value)() const) {
            return component<T, U>((function_type<T, U>) method_ptr_wrapper<T, U>(value));
        };
    };

    template <class TValue>
    struct type_map_base<TValue, util::TYPE_MEMBER_FUNCTION_POINTER> : type_map_member_function_helper<typename std::decay<TValue>::type> {};

    template <class T, class U>
    struct type_map_base<U (*)(T), util::TYPE_POINTER> : type_map_function_helper<U (*)(T)> {};

    template <class TValue>
    struct type_map : type_map_base<typename std::decay<TValue>::type, util::type_category<typename std::decay<TValue>::type>> {};

    template <class TValue>
    using sink_type = typename std::decay<typename type_map<TValue>::sink_t>::type;

    template <class TValue>
    using source_type = typename std::decay<typename type_map<TValue>::source_t>::type;

    template <class TValue>
    constexpr bool store_type = type_map<TValue>::store;

    template <class TValue>
    auto convert_type(const TValue &value) {
        return type_map<TValue>::convert(value);
    }
}


#endif //PIPELINE_TYPE_MAP_H
