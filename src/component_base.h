//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_COMPONENT_BASE_H
#define PIPELINE_COMPONENT_BASE_H


#include "impl/sink_base.h"
#include "impl/source_base.h"

namespace pipeline {
    template <class T, class U>
    class component_base : public impl::sink_base<T>, public impl::source_base<U> {
        // TODO: any core parts of a component not defined in sink_base or source_base
    };

    template <>
    class component_base<void, void>; // Invalid specialization, not defined
    
    template <class T>
    using sink_base = component_base<T, void>;
    
    template <class T>
    using source_base = component_base<void, T>;
}


#endif //PIPELINE_COMPONENT_BASE_H
