//
// Created by nmeritn on 2018-06-11.
//

#ifndef PIPELINE_COMPONENT_FACTORY_H
#define PIPELINE_COMPONENT_FACTORY_H


namespace pipeline {
    template <class F>
    class component_factory final {
    private:
        component_factory() = default;

    public:
        static constexpr component_factory<F> value = component_factory<F>();
    };
}


#endif //PIPELINE_COMPONENT_FACTORY_H
