//
// Created by nmeritn on 2018-06-15.
//

#ifndef PIPELINE_TIE_H
#define PIPELINE_TIE_H


#include "component.h"
#include "util/type_ternary.h"
#include "impl/tie_component.h"

namespace pipeline {
    template <bool aggressive_push = false, class ...Types>
    impl::tie_result<Types...> tie(const Types &...args) {
        return impl::create(new impl::tie_component<aggressive_push, component<impl::sink_type<Types>, impl::source_type<Types>>...>(impl::convert_type(args)...));
    }
}


#endif //PIPELINE_TIE_H
