//
// Created by nmeritn on 2018-06-07.
//

#ifndef PIPELINE_CREATE_H
#define PIPELINE_CREATE_H


#include "impl/create.h"

namespace pipeline {
    template <class TComponent, class ...TArgs>
    auto create(TArgs &&...args) {
        return impl::create(new TComponent(std::forward<TArgs>(args)...));
    }
}


#endif //PIPELINE_CREATE_H
