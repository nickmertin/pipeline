//
// Created by nmeritn on 2018-06-20.
//

#ifndef PIPELINE_CONVERT_H
#define PIPELINE_CONVERT_H


#include "impl/conversion_factory.h"

namespace pipeline {
    template <class U>
    constexpr auto convert = component_factory<impl::conversion_factory<U>>::value;
}


#endif //PIPELINE_CONVERT_H
