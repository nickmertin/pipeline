//
// Created by nmeritn on 2018-06-06.
//

#ifndef PIPELINE_COMPONENT_H
#define PIPELINE_COMPONENT_H


#include <memory>
#include <cstddef>
#include "impl/sink.h"
#include "impl/source.h"
#include "impl/data.h"
#include "function_transform.h"
#include "impl/function_type.h"
#include "impl/method_ptr_wrapper.h"

namespace pipeline {
    template <class, class>
    class component;

    namespace impl {
        template <class T, class U>
        component<T, U> create(component_base<T, U> *);

        template <bool, class, class, class ...>
        class tie_component_base;
    }

    template <class T, class U>
    class component final : public impl::sink<T, U>, public impl::source<T, U> {
    private:
        std::shared_ptr<impl::data<T, U>> _data;

        impl::sink_data<T, U> &get_sink() const {
            return *dynamic_cast<impl::sink_data<T, U> *>(_data.get());
        }

        impl::source_data<T, U> &get_source() const {
            return *dynamic_cast<impl::source_data<T, U> *>(_data.get());
        }

        explicit component(typename impl::component_data<T, U>::container *_container) : _data(new impl::data<T, U>(_container)) {}

        template <class A, class B>
        friend component<A, B> impl::create(component_base<A, B> *value);

        template <class, class>
        friend class impl::sink;

        template <class, class>
        friend class impl::source;

        template <bool, class, class, class ...>
        friend class impl::tie_component_base;

    public:
        component() noexcept : _data() {}

        component(const component<T, U> &other) noexcept : _data(other._data) {}

        component(component<T, U> &&other) noexcept : _data(std::move(other._data)) {}

        explicit component(nullptr_t) noexcept : _data() {}

        explicit component(const impl::function_type<T, U> &_function) noexcept : component(impl::create(new function_transform<T, U>(_function))) {}

        explicit component(impl::method_ptr_type<T, U> _function) noexcept : component((impl::function_type<T, U>) impl::method_ptr_wrapper<T, U>(_function)) {}

        component<T, U> &operator=(const component<T, U> &other) noexcept {
            _data = other._data;
            return *this;
        }

        component<T, U> &operator=(component<T, U> &&other) noexcept {
            _data = std::move(other._data);
            return *this;
        }

        component<T, U> &operator=(nullptr_t) noexcept {
            _data.reset();
            return *this;
        }

        operator bool() const {
            return (bool) _data;
        }

        template <class TComponent>
        TComponent *get() const {
            return dynamic_cast<TComponent *>(&_data->_container->get_sink());
        }
    };

    template <class T>
    using sink = component<T, void>;

    template <class T>
    using source = component<void, T>;

    using nothing = component<void, void>;
}


#endif //PIPELINE_COMPONENT_H
