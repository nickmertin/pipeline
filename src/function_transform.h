//
// Created by nmeritn on 2018-06-10.
//

#ifndef PIPELINE_FUNCTION_TRANSFORM_H
#define PIPELINE_FUNCTION_TRANSFORM_H


#include "transform_base.h"
#include "impl/function_type.h"

namespace pipeline {
    template <class T, class U = T>
    class function_transform final : public transform_base<T, U> {
    private:
        impl::function_type<T, U> _function;

        U transform(const T &value) override {
            return _function(value);
        }

    public:
        explicit function_transform(const impl::function_type<T, U> &_function) : _function(_function) {}
    };
}


#endif //PIPELINE_FUNCTION_TRANSFORM_H
