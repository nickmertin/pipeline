# Pipeline Library Specification

Pipeline is a library for C++ that supports the creation of "data networks" (pipelines), to control the flow of values between various components of the program logically and functionally.

## Concepts

The library is built around the concept of data sources and data sinks. Sources are structures that can push data to sinks; likewise, sinks are structures that can accept data from sources. This flow of data is controlled by binding sinks to sources; a sink can only be bound to one source, but many sinks can be bound to the same source, creating a n->1 relationship.

A pipeline component is an object that is used in the pipeline. A pipeline component may act as a source, a sink, or both (and in the case of both, not necessarily of the same data type).

# Types
Hierarchy diagram of core types

    impl::source_base<U>      impl::sink_base<T>
                \               /
                 \             /
                component_base<T, U>
    
    
    impl::source<T, U>      impl::sink<T, U>
                \            /
                 \          /
                component<T, U>

For the pipe operator, if either component is an rvalue reference, it should be moved to a new location internal to the created `component<A, B>` object (binding component), and destruction of the binding component should result in destruction of the internal component(s). Otherwise, the original components should be referenced directly, and destruction of either original component should result in destruction of the binding component. If at least one original component is not an rvalue reference, then the binding component should be held internally by that original component, with preference going to the sink.

# Using Sources and Sinks

In general, a value representing a sink can be bound to a value representing a source using a pipe symbol:

    source | sink;

Additionally, if a particular sink is also a source of the same data type, it may be appended to another source as a filter:

    source |= filter;

This modifies the source such that all values that it pushes are first pushed through the filter that is passed.
